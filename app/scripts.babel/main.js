import queryString from 'query-string';

const reportConnectParams = (connectParams) => {
    console.group('Context Parameters');
    Object.keys(connectParams).forEach((key) => {
        console.info(`%c${key}:`, 'font-weight: bold; color: #0049B0;', `${connectParams[key]}`);
    })
    console.groupEnd();
}

const report = (apContainer, loadTime) => {
    const link = document.createElement('a');
    link.href = apContainer.querySelector('.ap-content iframe').src;

    console.groupCollapsed(`Connect Analytics for %c${link.origin}`, 'font-weight: bold; color: #0049B0;');
    console.info(`%c${link.origin} iframe loaded in:`, 'font-weight: bold; color: #0049B0;', `${Math.round(loadTime)}ms`);
    console.info('%cApdex:', 'font-weight: bold; color: #0049B0;', loadTime < 1000 ? 1 : (loadTime > 4000 ? 0 : 0.5));
    reportConnectParams(queryString.parse(link.search));
    console.groupEnd();
}

const AP_CONTAINER_CLASS = '.ap-container';
document.querySelectorAll(AP_CONTAINER_CLASS).forEach((apContainer) => {

    const start = performance.now();

    const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
            if (mutation.attributeName === 'class') {
                const classList = mutation.target.classList;
                if (classList.contains('hidden') && start) {
                    report(apContainer, performance.now() - start);
                }
            } else if (mutation.attributeName === 'style' && mutation.target.style.display === 'none') {
                report(apContainer, performance.now() - start);
            }
        });
    });

    observer.observe(apContainer,  {
        childList: true,
        subtree: true,
        attributes: true
    });

});
